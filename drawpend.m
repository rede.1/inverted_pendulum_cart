function drawpend(state,m,M,l)
%% Borrowed Code from Steve Brunton for Pendulum Animation
% Minor modifications
x = state(1);
theta = state(3);

W = 1*(M/5);
H = 0.5*sqrt(M/5);
wr = 0.2;
mr = 0.3*sqrt(m);

y = wr/2+H/2;
pendx = x - l*sin(theta);
pendy = y + l*cos(theta);

plot([-10 10],[0 0],'k','LineWidth',2);
hold on;
plot([-10 10],[0 0],'k','LineWidth',2), hold on
rectangle('Position',[x-W/2,y-H/2,W,H],'Curvature',.1,'FaceColor',[.5 0.5 1],'LineWidth',1.5); % Draw cart
rectangle('Position',[x-.9*W/2,0,wr,wr],'Curvature',1,'FaceColor',[0 0 0],'LineWidth',1.5); % Draw wheel
rectangle('Position',[x+.9*W/2-wr,0,wr,wr],'Curvature',1,'FaceColor',[0 0 0],'LineWidth',1.5); % Draw wheel
plot([x pendx],[y pendy],'k','LineWidth',2); % Draw pendulum
rectangle('Position',[pendx-mr/2,pendy-mr/2,mr,mr],'Curvature',1,'FaceColor',[1 0.1 .1],'LineWidth',1.5);

axis([-5 5 -2 2.5]);, axis equal
set(gcf,'Position',[100 100 1000 400])
drawnow, hold off
end