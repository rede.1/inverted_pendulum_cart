
%% UMich Control Tutorial with Modifications 

%% Initialize Workspace
clear all, close all, clc

%% Dynamic Model
M = 5;
m = 1;
b = 1;
I = 1;
g = 9.8;
l = 1;

p = I*(M+m)+M*m*l^2; %denominator for the A and B matrices

A = [0      1              0           0;
     0 -(I+m*l^2)*b/p  (m^2*g*l^2)/p   0;
     0      0              0           1;
     0 -(m*l*b)/p       m*g*l*(M+m)/p  0];
 
B = [     0;
     (I+m*l^2)/p;
          0;
        m*l/p];
    
C = [1e-1 0 0 0;
     0 1e-5 0 0;
     0 0 1 0;
     0 0 0 1];
 
D = [0;
     0;
     0;
     0];

 %% Open Loop State-Space Form
states = {'x' 'x_dot' 'phi' 'phi_dot'};
inputs = {'u'};
outputs = {'x'; 'x_dot'; 'phi'; 'phi_dot'};
y0 = [0 0 pi/12 0]; % initial conditions

sys_ss = ss(A,B,C,D,'statename',states,'inputname',inputs,'outputname',outputs);

%% Basic Analysis of Open Loop State-Space Form
poles = eig(A);

co = ctrb(sys_ss);
controllability = rank(co);

%% LQR Implementation and Tuning
Q = C'*C;
R = 0.000001;
K = lqr(A,B,Q,R);
Ac = [(A-B*K)];
Bc = [B];
Cc = [C];
Dc = [D];


states = {'x' 'x_dot' 'phi' 'phi_dot'};
inputs = {'r'};
outputs = {'x'; 'x_dot'; 'phi'; 'phi_dot'};


sys_cl = ss(Ac,Bc,Cc,Dc,'statename',states,'inputname',inputs,'outputname',outputs);
%% Analysis of Closed Loop System

% Step Response with Initial Conditions
t = 0:0.01:5;
r = ones(size(t));
[y,t,x]=lsim(sys_cl,r,t,y0);
figure(1);
plot(t,y(:,1),t,y(:,2),t,y(:,3),t,y(:,4));
legend('x','xdot','phi','phidot');
title('Step Response with LQR Control');
xlim([0,5]);
ylim([-1,1]);
xlabel('t (seconds)');
ylabel('y (state variable)');

% Animation
figure(2);
for k=1:100:length(t)
    drawpend(y(k,:),m,M,l);
    xlabel('x'); ylabel('y');
    pause(0.1);
end